import React, { Component } from 'react';
import './App.css';

import axios from "axios";
import fontColorContrast from "font-color-contrast";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      closestColor: "#FFF",
      closestColorName: "",
      queryColor: "#FFF"
    }
  }

  stripColorNotation = color => {
    color = color.replace(/ /, "");
    color = color.replace(/#/, "");
    color = color.replace("rgb(", "");
    color = color.replace(")", "");
    return color;
  }
  
  isHex = (query) => {
    return query.match(/^#?([a-fA-F0-9]{6}|[a-fA-F0-9]{3})$/) !== null;
  }

  isRGB = query => {
    return query.match(/(?=[0-2]{0,1}[0-9]{0,2},[0-2]{0,1}[0-9]{0,2},[0-2]{0,1}[0-9]{0,2})([0-9]{1,3},[0-9]{1,3},[0-9]{1,3})/)
  }

  isColor = query => {
    return (this.isHex(query) || this.isRGB(query));
  }

  getBGColor = color => {
    color = this.stripColorNotation(color);
    if (this.isRGB(color)) {
      return `rgb(${color})`;
    } else if (this.isHex(color)) {
      return `#${color}`;
    }
  }

  getClosestColor = (e) => {
    let queryColor = this.stripColorNotation(e.target.value);

    if (queryColor && queryColor.length !== 0 && this.isColor(queryColor)) {
      axios.get(`${process.env.REACT_APP_API}/copic/${queryColor}`).then(res => {
        const closestColor = res.data.value;
        const closestColorName = res.data.name;
        this.setState({closestColor, closestColorName, 
          queryColor: this.isHex(queryColor) ? `#${queryColor}` : `rgb(${queryColor})`});
      })
    } else {
      this.setState({closestColor: "#FFF", closestColorName: "", queryColor: e.target.value});
    }

  }

  getTextColor = bg => {
    bg = this.stripColorNotation(bg);

    if (this.isHex(bg)) {
      if (bg.length === 3) {
        bg = `#${bg[0]}${bg[0]}${bg[1]}${bg[1]}${bg[2]}${bg[2]}`;
      } else {
        bg = `#${bg}`;
      }
    } else if (this.isRGB(bg)) {
      const c = bg.split(",");
      bg = [ c[0], c[1], c[2] ];
    }
    return fontColorContrast(bg);
  }

  render() {
    return (
      <div className="App" /*style={{backgroundColor: this.state.closestColor}}*/>
        <div className="App-body">
          <h1>Copic Color Match</h1>
          <input 
            className={this.state.queryColor === "#FFF" || this.isColor(this.state.queryColor) ? "valid" : "invalid"} 
            placeholder="#FFFFFF" 
            type="text" 
            onKeyUp={e => this.getClosestColor(e)} />
          <div className="display-colors" style={{opacity: this.state.closestColorName === "" ? 0 : 1}}>
            <div style={{backgroundColor: this.getBGColor(this.state.queryColor), color: this.getTextColor(this.state.queryColor)}}>
              Your Color
              <small>{this.state.queryColor}</small>
            </div>
            <div placeholder="#FFFFFF" style={{backgroundColor: this.getBGColor(this.state.closestColor), color: this.getTextColor(this.state.closestColor)}}>
              Copic Color
              <small>{this.state.closestColorName}</small>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default App;
